library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity multiplier is
    generic (N: Positive := 8; REC_LIMIT: Positive := 2);
    port ( 
	a : in unsigned(N-1 downto 0);
	b : in unsigned(N-1 downto 0);
	x : out unsigned(2*N-1 downto 0)
   );
end multiplier;

--architecture Behavioral of multiplier is
--begin 
--	x <= a * b; 
--end Behavioral;

architecture karatsuba of multiplier is
	component multiplier is
	    generic (N: Positive := 8; REC_LIMIT: Positive := 2);
	    port ( 
		a : in unsigned(N-1 downto 0);
		b : in unsigned(N-1 downto 0);
		x : out unsigned(2*N-1 downto 0)
	   );
	end component;

begin
	recursive_structure:
	if N > REC_LIMIT generate

		constant M : positive := N/2;

		--signal ah: unsigned(N-1 downto M);
		signal ah: unsigned(M-1 downto 0);
		signal al: unsigned(M-1 downto 0);
        -- a_add is 2 * N/2 = M bit + 1
        -- Add 1 bit to make it always even
		signal a_add: unsigned(M+1 downto 0); 

		--signal bh: unsigned(N-1 downto M);
		signal bh: unsigned(M-1 downto 0);
		signal bl: unsigned(M-1 downto 0);
		signal b_add: unsigned(M+1 downto 0);

		signal tmp: unsigned(2*(M + 2) - 1 downto 0);

		signal z0: unsigned(2*M-1 downto 0);
		signal z2: unsigned(2*M-1 downto 0);

		signal z0_ext: unsigned(2*N-1 downto 0);
		signal z1_ext: unsigned(2*N-1 downto 0);
		signal z2_ext: unsigned(2*N-1 downto 0);

	begin

		ah <= a(a'high downto M);
		al <= a(M-1 downto 0);
		bh <= b(b'high downto M);
		bl <= b(M-1 downto 0);
		
		karatsuba0: multiplier 
			generic map(M, REC_LIMIT) port map(al, bl, z0);

		-- a_add <= ('0' & ah) + al;
		-- b_add <= ('0' & bh) + bl;
		a_add <= ('0' & (('0' & ah) + al));
		b_add <= ('0' & (('0' & bh) + bl));
		karatsuba1: multiplier
			generic map(M+2, REC_LIMIT) port map(a_add, b_add, tmp);
		
		karatsuba2: multiplier
			generic map(M, REC_LIMIT) port map(ah, bh, z2);

		z1_ext <= resize(tmp - z2 -z0, x'length);

		z0_ext <= resize(z0, x'length);
		z2_ext <= resize(z2, x'length);
		x <= (z2_ext sll 2*M) + (z1_ext sll M) + z0_ext;
	end generate;

	base_case: if N <= REC_LIMIT generate
		simple_mult: x <= a * b;
	end generate;
end karatsuba;

configuration recursive_karatsuba of multiplier is
	for karatsuba
		for recursive_structure
			for all: multiplier
			use entity work.multiplier(karatsuba);
			end for;
		end for;
	end for;
end recursive_karatsuba;
