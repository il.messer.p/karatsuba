# This file is public domain, it can be freely copied without restrictions.
# SPDX-License-Identifier: CC0-1.0
# Simple tests for a multiplier module
import os
import random
import sys
from pathlib import Path

import cocotb
from cocotb.runner import get_runner
from cocotb.triggers import Timer

from multiplier_model import multiplier_model

#if cocotb.simulator.is_running():
#    from multiplier_model import multiplier_model


@cocotb.test()
async def multiplier_basic_test(dut):
    """Test for 5 x 10"""

    A = 5
    B = 10

    dut.a.value = A
    dut.b.value = B

    await Timer(2, units="ns")

    assert dut.x.value == multiplier_model(A, B), f"Multiplier result is incorrect: {dut.x.value} != {A * B}"


@cocotb.test()
async def multiplier_randomised_test(dut):
    """Test for multiplying 2 random numbers multiple times"""

    for i in range(10000):

        A = random.randint(0, 255)
        B = random.randint(0, 255)
        expected_out = multiplier_model(A, B)

        dut.a.value = A
        dut.b.value = B

        await Timer(2, units="ns")

        assert dut.x.value == expected_out, "Randomised test failed with: {A} * {B} = {X}".format(
            A=dut.a.value, B=dut.b.value, X=dut.x.value
        )


def test_multiplier_runner():
    """Simulate the multiplier using the Python runner.
    This file can be run directly or via pytest discovery.
    """
    hdl_toplevel_lang = os.getenv("HDL_TOPLEVEL_LANG", "vhdl")
    sim = os.getenv("SIM", "ghdl")

    proj_path = Path(__file__).resolve().parent.parent
    # equivalent to setting the PYTHONPATH environment variable
    sys.path.append(str(proj_path / "model"))

    verilog_sources = []
    vhdl_sources = []

    if hdl_toplevel_lang == "verilog":
        verilog_sources = [proj_path / "rtl" / "multiplier.sv"]
    else:
        vhdl_sources = [proj_path / "rtl" / "multiplier.vhd"]

    # equivalent to setting the PYTHONPATH environment variable
    sys.path.append(str(proj_path / "tests"))

    runner = get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
        vhdl_sources=vhdl_sources,
        hdl_toplevel="multiplier",
        always=True,
    )
    runner.test(hdl_toplevel="multiplier", test_module="test_multiplier")


if __name__ == "__main__":
    test_multiplier_runner()
